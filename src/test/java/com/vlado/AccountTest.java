package com.vlado;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.*;

/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */

public class AccountTest {


    @Mock
    private TransactionRepository transactionRepository;

    private Account account ;

    @Mock
    private StatementPrinter statementPrinter;


    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        account = new Account(transactionRepository, statementPrinter);

    }

    @Test
    void shouldStoreADepositTransaction() {
        account.deposit(100.00);
        verify(transactionRepository).addDeposit(100.00);
    }


    @Test
    void shouldStoreAWithdrawTransaction() {
        account.withdraw(100.00);
        verify(transactionRepository).addWithdraw(100.00);
    }


    @Test
    void shouldPrintAstatement() {
        List<Transaction> transtactions = List.of(new Transaction("12/05/2021",100.00));
        BDDMockito.given(transactionRepository.allTransactions()).willReturn(transtactions);

        account.printStatement();
        verify(statementPrinter).print(transtactions);
    }
}
