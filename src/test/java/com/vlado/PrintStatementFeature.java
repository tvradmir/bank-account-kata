package com.vlado;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.BDDMockito;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */
public class PrintStatementFeature {


    public static final String LINE_SEPARATOR = System.lineSeparator();
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();

    private Account account;

    @Mock
    private DateFormatter dateFormatter;


    @BeforeEach
    public void setUp() {
        MockitoAnnotations.openMocks(this);


        TransactionRepository transactionRepository = new TransactionRepository(dateFormatter);
        StatementPrinter statementPrinter = new StatementPrinter();
        account = new Account(transactionRepository, statementPrinter);
        System.setOut(new PrintStream(outputStreamCaptor));
    }



    @Test
    void should_print_statement_contains_all_transactions() {

        given(dateFormatter.todayAsString())
                .willReturn("10/07/2021","25/07/2021","29/07/2021");


        account.deposit(1000.00);
        account.withdraw(100.00);
        account.deposit(500.00);

        account.printStatement();

        String expectedStatement = "Date || Amount || Balance"+ LINE_SEPARATOR
                        +"29/07/2021 || 500.00 || 1400.00"+ LINE_SEPARATOR
                        +"25/07/2021 || -100.00 || 900.00"+ LINE_SEPARATOR
                        +"10/07/2021 || 1000.00 || 1000.00";

        assertThat(outputStreamCaptor.toString().trim()).isEqualTo(expectedStatement);



    }


    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }
}
