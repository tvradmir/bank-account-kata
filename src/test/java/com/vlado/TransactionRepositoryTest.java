package com.vlado;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.assertj.core.api.Assertions.*;
import static org.mockito.BDDMockito.*;

/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */
public class TransactionRepositoryTest {


    public static final String TODAY = "12/05/2021";
    private TransactionRepository transactionRepository;

    @Mock
    private DateFormatter dateFormatter;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        transactionRepository = new TransactionRepository(dateFormatter);

        given(dateFormatter.todayAsString()).willReturn(TODAY);

    }

    @Test
    void shouldStoreAndCreateDepositTransaction() {


        transactionRepository.addDeposit(100.0);

        List<Transaction> transactions = transactionRepository.allTransactions();
        assertThat(transactions).hasSize(1);
        assertThat(transactions.get(0)).isEqualTo(transaction(TODAY,100.00));

    }

    @Test
    void shouldStoreAndCreateWithdrawTransaction() {

        transactionRepository.addWithdraw(100.0);

        List<Transaction> transactions = transactionRepository.allTransactions();

        assertThat(transactions).hasSize(1);
        assertThat(transactions.get(0)).isEqualTo(transaction(TODAY,-100.00));

    }

    private Transaction transaction(String date, double amount) {
        return  new Transaction(date, amount);
    }
}
