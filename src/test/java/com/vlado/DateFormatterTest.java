package com.vlado;

import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.LocalDate;


/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */
class DateFormatterTest {

    @Test
    void should_today_date_in_dd_MM_yyyy_format() {

        DateFormatter dateFormatter = new TestBleDateFormatter();
        Assertions.assertThat(dateFormatter.todayAsString()).isEqualTo("28/07/2021");
    }

    private class TestBleDateFormatter extends DateFormatter {
        @Override
        protected LocalDate getTodayDate() {
            return  LocalDate.of(2021,07,28);
        }
    }
}