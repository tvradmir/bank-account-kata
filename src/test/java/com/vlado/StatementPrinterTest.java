package com.vlado;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.MockitoAnnotations;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.InstanceOfAssertFactories.ARRAY;


/**
 * @author Vlado on 30/07/2021
 * @project bank-account
 */
class StatementPrinterTest {


    private static final List<Transaction> NO_TRANSACTIONS = Collections.EMPTY_LIST;
    public static final String LINE_SEPARATOR = System.lineSeparator();
    private final PrintStream standardOut = System.out;
    private final ByteArrayOutputStream outputStreamCaptor = new ByteArrayOutputStream();
    private StatementPrinter statementPrinter;

    @BeforeEach
    public void setUp() {
        statementPrinter = new StatementPrinter();
        System.setOut(new PrintStream(outputStreamCaptor));
    }

    @Test
    void should_always_print_the_header() {

        statementPrinter.print(NO_TRANSACTIONS);

        String header = "Date || Amount || Balance";
        assertThat(outputStreamCaptor.toString().trim()).isEqualTo(header);

    }


    @Test
    void should_print_transactions_in_reverse_chronological_order() {

        List<Transaction> transactions  = transactionsContains(
                deposit("10/07/2021",1000.00 ),
                withdraw("25/07/2021",100.00),
                deposit("29/07/2021",500.00 )
        ) ;

        statementPrinter.print(transactions);

        String expectedStatement =
                 "Date || Amount || Balance"+ LINE_SEPARATOR
                +"29/07/2021 || 500.00 || 1400.00"+LINE_SEPARATOR
                +"25/07/2021 || -100.00 || 900.00"+LINE_SEPARATOR
                +"10/07/2021 || 1000.00 || 1000.00";
        assertThat(outputStreamCaptor.toString().trim()).isEqualTo(expectedStatement);
    }

    private List<Transaction> transactionsContains(Transaction... transactions) {
        return List.of(transactions);
    }

    private Transaction withdraw(String date, double amount) {
        return new Transaction(date,-amount);
    }

    private Transaction deposit(String date, double amount) {
        return new Transaction(date, amount);
    }

    @AfterEach
    public void tearDown() {
        System.setOut(standardOut);
    }
}