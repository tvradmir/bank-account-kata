package com.vlado;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */
public class DateFormatter {

    public static final DateTimeFormatter DD_MM_YYYY = DateTimeFormatter.ofPattern("dd/MM/yyyy");

    public String todayAsString() {
        return getTodayDate().format(DD_MM_YYYY);
    }

    protected LocalDate getTodayDate() {
        return LocalDate.now();
    }
}
