package com.vlado;

/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */
public class Account {


    private TransactionRepository transactionRepository;
    private StatementPrinter statementPrinter;

    public Account(TransactionRepository transactionRepository, StatementPrinter statementPrinter) {

        this.transactionRepository = transactionRepository;
        this.statementPrinter = statementPrinter;
    }

    public void deposit(double amount) {
        transactionRepository.addDeposit(amount);
    }

    public void withdraw(double amount) {
        transactionRepository.addWithdraw(amount);
    }

    public void printStatement() {
        statementPrinter.print(transactionRepository.allTransactions());
    }
}
