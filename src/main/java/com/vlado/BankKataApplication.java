package com.vlado;

/**
 * @author Vlado on 30/07/2021
 * @project bank-account
 */
public class BankKataApplication {

    public static void main(String[] args) {
        DateFormatter dateFormatter = new DateFormatter();
        TransactionRepository transactionRepository = new TransactionRepository(dateFormatter);
        StatementPrinter statementPrinter = new StatementPrinter();
        Account account = new Account(transactionRepository, statementPrinter);

        account.deposit(1000);
        account.withdraw(100);
        account.deposit(500);

        account.printStatement();
    }
}
