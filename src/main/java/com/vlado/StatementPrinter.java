package com.vlado;


import org.w3c.dom.ls.LSOutput;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;


/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */
public class StatementPrinter {

    public static final String STATEMENT_HEADER = "Date || Amount || Balance";
    private DecimalFormat decimalFormat = new DecimalFormat("##.00", new DecimalFormatSymbols(Locale.ENGLISH));


    public void print(List<Transaction> transtactions) {

        printLine(STATEMENT_HEADER);
        printStatementLines(transtactions);

    }

    private void printLine(String line) {
        System.out.println(line);
    }

    private void printStatementLines(List<Transaction> transtactions) {
        AtomicInteger runningBalance = new AtomicInteger(0);
        transtactions.stream()
               .map(transaction -> statementLine(transaction, runningBalance))
               .collect(Collectors.toCollection(LinkedList::new))
               .descendingIterator()
               .forEachRemaining(this::printLine);
    }

    private String statementLine(Transaction transaction, AtomicInteger runningBalance) {
        return transaction.getDate()
                +" || "
                +decimalFormat.format(transaction.getAmount())
                +" || "
                + decimalFormat.format(runningBalance.addAndGet((int) transaction.getAmount()));
    }

}
