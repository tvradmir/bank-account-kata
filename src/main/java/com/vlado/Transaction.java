package com.vlado;

import java.util.Objects;

/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */
public class Transaction {
    private String date;
    private double amount;

    public Transaction(String date, double amount) {
        this.date = date;
        this.amount = amount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Transaction that = (Transaction) o;
        return Double.compare(that.amount, amount) == 0 &&
                date.equals(that.date);
    }

    @Override
    public int hashCode() {
        return Objects.hash(date, amount);
    }

    public String getDate() {
        return date;
    }

    public double getAmount() {
        return amount;
    }
}
