package com.vlado;


import java.util.ArrayList;
import java.util.List;

import static java.util.Collections.*;

/**
 * @author Vlado on 29/07/2021
 * @project bank-account
 */
public class TransactionRepository {
    private DateFormatter dateFormatter;
    private List<Transaction> transactions = new ArrayList<>();

    public TransactionRepository(DateFormatter dateFormatter) {
        this.dateFormatter = dateFormatter;
    }

    public void addDeposit(double amount) {
        Transaction deposit = new Transaction(dateFormatter.todayAsString(), amount);
        transactions.add(deposit);
    }

    public void addWithdraw(double amount) {
        Transaction withdraw = new Transaction(dateFormatter.todayAsString(), -amount);
        transactions.add(withdraw);
    }

    public List<Transaction> allTransactions() {
        return unmodifiableList(transactions);
    }
}
