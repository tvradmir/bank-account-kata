# Your Task
Your bank is tired of its mainframe COBOL accounting software and they hired both of you for a greenfield project in - what a happy coincidence

your favorite programming language!
Your task is to show them that your TDD-fu and your new-age programming language can cope with good ole’ COBOL!

# Requirements
Write a class Account that offers the following methods 
void deposit(int) 
void withdraw(int) 
void printStatement()

```
public interface AccountService
{
    void deposit(int amount) 
    void withdraw(int amount) 
    void printStatement()
}
```

An example statement would be:

```
Date       || Amount ||  Balance
14/01/2012 || -500   || 2500
13/01/2012 || 2000   || 3000
10/01/2012 || 1000   || 1000

```